import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  if (!fighter) {
    return createElement({
      tagName: 'div'
    });
  }
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

const fighterImg = createFighterImage(fighter);
const fighterDetails = createElement({
  tagName: 'div',
  className: ` fighter-preview___details ${positionClassName}`,
});
  const fighterName = createElement({
    tagName: 'h3',
    className: `fighter-preview___name`,
  });
  fighterName.innerHTML = fighter.name;

  const fighterHealth = createElement({
    tagName: 'div',
  });
  fighterHealth.innerHTML = `Health: ${fighter.health}`;

  const fighterAttack = createElement({
    tagName: 'div',
  });
  fighterAttack.innerHTML = `Attack: ${fighter.attack}`;

  const fighterDefense = createElement({
    tagName: 'div',
  });
  fighterDefense.innerHTML = `Defense: ${fighter.defense}`;

  fighterDetails.append(fighterName, fighterHealth, fighterAttack, fighterDefense);
  fighterElement.append(fighterDetails, fighterImg);
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
