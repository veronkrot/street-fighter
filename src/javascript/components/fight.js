import { controls } from '../../constants/controls';

const KEY_LISTENER_EVENT = 'keyup';
const CRITICAL_HIT_INTERVAL = 10 * 1000; // 10 seconds to milliseconds

const copyObject = (obj) => {
  return JSON.parse(JSON.stringify(obj));
};
const firstFighterBarId = '#left-fighter-indicator';
const secondFighterBarId = '#right-fighter-indicator';

export async function fight(firstFighter, secondFighter) {
  const firstFighterCopy = copyObject(firstFighter);
  const secondFighterCopy = copyObject(secondFighter);
  let hasWinner = false;
  let winner = null;
  let key = [];

  document.onkeyup = document.onkeydown = function(e) {
    key[e.code] = e.type === KEY_LISTENER_EVENT;
  };
  const attack = (first, second, isCritical) => {
    const attackDamage = first.attack;
    if (isCritical) {
      first.attack *= 2;
    }
    second.health -= getHitPower(first);
    if (second.health <= 0) {
      hasWinner = true;
      winner = first;
    }
    if (isCritical) {
      first.attack = attackDamage;
    }
  };

  const damage = (first, second) => {
    first.health -= getDamage(second, first);
    if (first.health <= 0) {
      hasWinner = true;
      winner = second;
    }
  };

  const updateHealthBar = (fighter, isFirstFighter) => {
    let healthBarIdToUse = isFirstFighter ? firstFighterBarId : secondFighterBarId;
    let currentPercentageHP = (100 * fighter.health) / fighter.maxHP;
    if (currentPercentageHP < 0) {
      currentPercentageHP = 0;
    }
    let healthBar = document.querySelector(healthBarIdToUse);
    healthBar.style.width = currentPercentageHP + '%';
  };

  const criticalIntervals = {
    first: 0,
    second: 0
  }

  const isCriticalHitAllowed = (isFirst) => {
    const date = Date.now();
    let interval = isFirst ? criticalIntervals.first : criticalIntervals.second;
    if ((date - interval) >= CRITICAL_HIT_INTERVAL) {
      if (isFirst) {
        criticalIntervals.first = date;
      } else {
        criticalIntervals.second = date;
      }
      return true;
    }
    return false;
  }

  const listener = () => {
    if (key[controls.PlayerOneAttack] && key[controls.PlayerTwoBlock]) {
      damage(secondFighterCopy, firstFighterCopy);
      key = [];
    } else if (key[controls.PlayerTwoAttack] && key[controls.PlayerOneBlock]) {
      damage(firstFighterCopy, secondFighterCopy);
      key = [];
    } else if (key[controls.PlayerOneAttack]) {
      attack(firstFighterCopy, secondFighterCopy);
      key = [];
    } else if (key[controls.PlayerTwoAttack]) {
      attack(secondFighterCopy, firstFighterCopy);
      key = [];
    } else if (controls.PlayerOneCriticalHitCombination.every(x => key[x])) {
      if (isCriticalHitAllowed(true)) {
        attack(firstFighterCopy, secondFighterCopy, true);
      }
      key = [];
    } else if (controls.PlayerTwoCriticalHitCombination.every(x => key[x])) {
      if (isCriticalHitAllowed(false)) {
        console.log('critical allowed');
        attack(secondFighterCopy, firstFighterCopy, true);
      }
      key = [];
    }
    updateHealthBar(firstFighterCopy, true);
    updateHealthBar(secondFighterCopy, false);
  };

  document.addEventListener(KEY_LISTENER_EVENT, listener);
  return new Promise((resolve) => {
    setInterval(() => {
      if (hasWinner) {
        document.removeEventListener(KEY_LISTENER_EVENT, listener);
        resolve(winner);
      }
    }, 100);
  });
}

const randomNum = (max, min) => {
  return Math.random() * (max - min) + min;
};

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if (damage < 0) {
    return 0;
  }
  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = randomNum(1, 2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = randomNum(1, 2);
  return fighter.defense * dodgeChance;
}
