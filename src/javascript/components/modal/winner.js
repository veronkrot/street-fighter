import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import { fighterService } from '../../services/fightersService';
import { createFighters } from '../fightersView';
import App from '../../app';

export function showWinnerModal(fighter) {
  const winner = {
    title: `${fighter.name} wins!`,
    bodyElement: createFighterImage(fighter),
    onClose: async function() {
      const arena = document.querySelector('.arena___root');
      const fighters = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);

      App.rootElement.appendChild(fightersElement);
      arena.remove();
    }
  };
  return showModal(winner);
}
